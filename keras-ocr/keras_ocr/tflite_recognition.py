import tensorflow as tf
from tensorflow import keras
import numpy as np
import cv2
import os
import time


def run_tflite_model(input):

    #path = "/home/jeremy/Documents/stage/keras/keras-ocr/ocr_float16.tflite"
    path = "C:/Users/juston/Documents/kerasocr/keras-ocr/ocr_float16.tflite"
    interpreter = tf.lite.Interpreter(model_path=path)
    interpreter.allocate_tensors()

    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    output = []
    start = time.time()
    for input_data in input:
        input_data = input_data[np.newaxis]
        input_data = np.expand_dims(input_data, 3)
        input_data = input_data.astype('float32')/255
        
        input_shape = input_details[0]['shape']
        
        interpreter.allocate_tensors()
        interpreter.set_tensor(input_details[0]['index'], input_data)

        interpreter.invoke()
        out = interpreter.get_tensor(output_details[0]['index'])
        out = out.tolist()
        out = out[0]
        output.append(out)

    end = time.time()
    print("OCR : ", end-start)
    return output