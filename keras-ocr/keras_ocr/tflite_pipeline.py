import typing
import string

import tensorflow as tf
from tensorflow import keras
import numpy as np
import cv2
from tflite_recognition import run_tflite_model
from tflite_detection import detect_boxes
from tools import read, warpBox


alphabet = string.digits + string.ascii_lowercase
blank_label_idx = len(alphabet)

def recognize_from_boxes(
        images, box_groups
    ) -> typing.List[typing.List[str]]:
        """Recognize text from images using lists of bounding boxes.

        Args:
            images: A list of input images, supplied as numpy arrays with shape
                (H, W, 3).
            boxes: A list of groups of boxes, one for each image
        """
        assert len(box_groups) == len(
            images
        ), "You must provide the same number of box groups as images."
        
        crops = []
        start_end: typing.List[typing.Tuple[int, int]] = []
        for image, boxes in zip(images, box_groups):
            image = read(image)
            if image.shape[-1] == 3:
                # Convert color to grayscale
                image = cv2.cvtColor(image, code=cv2.COLOR_RGB2GRAY)
            for box in boxes:
                crops.append(
                    warpBox(
                        image=image,
                        box=box,
                        target_height=31,
                        target_width=200,
                    )
                )
            start = 0 if not start_end else start_end[-1][1]
            start_end.append((start, start + len(boxes)))
        if not crops:
            return [[]] * len(images)
        
        X = np.float32(crops)
        
        predictions = [
            "".join(
                [
                    alphabet[idx]
                    for idx in row
                    if idx not in [blank_label_idx, -1]
                ]
            )
            for row in run_tflite_model(X)
        ]
        return [predictions[start:end] for start, end in start_end]

images_path = ["600x800.jpg"]
images = []
boxes = []

for i in range(len(images_path)):
    result = detect_boxes(images_path[i])
    images.append(result[0])
    boxes.append(result[1])

print(recognize_from_boxes(images, boxes))