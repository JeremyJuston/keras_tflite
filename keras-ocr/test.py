import numpy as np
import time
import keras_ocr

def test_pipeline():
    pipeline = keras_ocr.pipeline.Pipeline()
    start = time.time()
    image = keras_ocr.tools.read("600x800.jpg")
    
    # Predictions is a list of (text, box) tuples.
    predictions = pipeline.recognize(images=[image])
    end = time.time()
    print(predictions)
    print("TIME : ", end-start)

test_pipeline()